## Introduction

This is the registration server used for the join.codeberg.org form.

## Security

Incoming user data from forms is immediately encrypted using GnuPG Public-Key Encryption. No private key is ever stored on the server.

Encrypted registration records are sent via email to the registration accounts, from where they are fetched and processed off-line.


## Files, Overview

```
README.md                                ## This file
main.go                                  ## Source code of the reg-server 
etc/                                     ## The config file template folder, copied to target host at deployment
etc/systemd/system/reg-server.service    ## Systemd service to launch, monitor and restart the reg-service
etc/reg-server/public-key.asc.gpg        ## The public GnuPG key used for encryption on the server
etc/reg-server/static/                   ## Static webapp content
etc/reg-server/templates/                ## Language-dependent templates. Translations welcome.
```

## Build+test locally

```shell
make BUILDDIR=/tmp/build && ./reg-server
/tmp/build/gitea/bin/reg-server
```

Then point your browser at http://localhost:5000/

## Build+deploy

You may want to start `ssh-agent` and do `ssh-add` before calling `make deployment`, to avoid typing your ssh passphrase several times. Then do:

```shell
make -C HOSTNAME_FQDN=<hostname> deployment
```

